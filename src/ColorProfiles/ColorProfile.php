<?php

namespace ColorProfiles;

/**
 * Class ColorProfile
 *
 * @package ColorProfiles\Console
 */
final class ColorProfile
{

    const TYPE_ICC = 'icc';
    const TYPE_IPTC = 'iptc';

    const COLORSPACE_RGB = 'rgb';
    const COLORSPACE_CMYK = 'cmyk';

    private $file;
    private $type = self::TYPE_ICC;
    private $name;
    private $colorSpace;

    /**
     * @param $file
     * @param $colorSpace
     * @param $name
     * @param $type
     */
    public function __construct($file, $colorSpace, $name, $type = self::TYPE_ICC)
    {
        $this->file = $file;
        $this->colorSpace = strtolower($colorSpace);
        $this->name = $name;
        $this->type = strtolower($type);
    }

    /**
     * @return string
     */
    public function getColorSpace()
    {
        return $this->colorSpace;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getProfile()
    {
        return file_get_contents($this->file);
    }

    /**
     * @return mixed
     */
    function __toString()
    {
        return $this->getName();
    }

}