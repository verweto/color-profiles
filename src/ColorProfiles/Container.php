<?php

namespace ColorProfiles;

/**
 * Class Container
 *
 * @package ColorProfiles
 */
final class Container
{

    /**
     * @var ColorProfile[]|\ArrayObject
     */
    private $colorProfiles = [];

    /**
     * @param ColorProfile[] $colorProfiles
     */
    public function __construct($colorProfiles)
    {
        $this->colorProfiles = ($colorProfiles instanceof \ArrayObject) ? $colorProfiles : new \ArrayObject($colorProfiles);
    }

    /**
     * @return ColorProfile[]
     */
    public function getIterator()
    {
        return $this->colorProfiles;
    }

    /**
     * @param string $colorSpace
     *
     * @return Container
     */
    public function findByColorSpace($colorSpace)
    {
        return new Container(array_filter((array) $this->colorProfiles,
            function(ColorProfile $colorProfile) use ($colorSpace) {
                return strtolower($colorProfile->getColorSpace()) == strtolower($colorSpace);
            }
        ));
    }

}