<?php

namespace ColorProfiles\Console\Command;

use ColorProfiles\ColorProfile;
use ColorProfiles\Container;
use ColorProfiles\Converter\ConverterInterface;
use Symfony\Component\Console\Command\Command as CliCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class Convert extends CliCommand
{

    /**
     * @var array
     */
    public static $converters = [
        '\ColorProfiles\Converter\CmykToRgbConverter',
    ];

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this
            ->setName('convert')
            ->setDescription('Convert an image with a set of image profiles.')
            ->addArgument(
                'source',
                InputArgument::REQUIRED,
                'Source image'
            )
            ->addArgument(
                'destination',
                InputArgument::OPTIONAL,
                'Destination folder',
                sys_get_temp_dir()
            )
            ->addOption(
                'profile',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Specify a profiles namespace',
                'adobe'
            )
            ->addOption(
                'output-type',
                'o',
                InputOption::VALUE_OPTIONAL,
                'Specify an output type',
                'jpg'
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $image = $input->getArgument('source');
        $destination = $input->getArgument('destination');
        $profileNamespace = $input->getOption('profile');
        $outputType = strtolower($input->getOption('output-type'));

        $image = $this->loadImage($image);
        $destination = $this->createDestination($destination);
        $colorProfiles = $this->loadProfiles($profileNamespace);
        $converters = $this->loadConverters($image, $destination);

        // Print some extra file data:
        $this->printImageInfo($output, $image);

        // Start converters
        $hasConvertedFiles = false;
        $output->writeln('Converting ...');
        foreach ($converters as $converter) {
            if (!$converter->canConvertImage()) {
                continue;
            }

            // Convert files:
            $files = $converter->convert($colorProfiles, $outputType);
            foreach ($files as $file) {
                $output->writeln(sprintf('<fg=green>Created %s</fg=green>', $file));
            }

            // Finish command:
            $hasConvertedFiles = true;
            break;
        }

        if (!$hasConvertedFiles) {
            throw new \RuntimeException('No valid converter could be found.');
        }
    }

    /**
     * @param $image
     *
     * @return \Imagick
     * @throws \RuntimeException
     */
    protected function loadImage($image)
    {
        $fileSystem = new Filesystem();
        if (!$fileSystem->exists($image)) {
            throw new \RuntimeException('Unable to locate image: ' . $image);
        }

        return new \Imagick($image);
    }

    /**
     * @param $destination
     *
     * @return string
     */
    protected function createDestination($destination)
    {
        $filesystem = new Filesystem();
        $filesystem->mkdir($destination);
        return realpath($destination);
    }

    /**
     * @param $profileNamespace
     *
     * @return Container
     */
    protected function loadProfiles($profileNamespace)
    {
        $finder = new Finder();
        $finder->name('*.icc');
        $finder->in(getcwd());

        $colorProfiles = [];
        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder->files() as $file) {

            $path = $file->getRealPath();
            $colorSpace = basename($file->getPath());
            $name = pathinfo($path, PATHINFO_FILENAME);
            $type = pathinfo($path, PATHINFO_EXTENSION);

            $colorProfiles[] = new ColorProfile($path, $colorSpace, $name, $type);
        }

        return new Container($colorProfiles);
    }

    /**
     * @param \Imagick $image
     * @param $destination
     *
     * @return ConverterInterface[]
     */
    protected function loadConverters($image, $destination)
    {
        $converters = [];
        foreach (self::$converters as $converterClass) {
            $converters[] = new $converterClass($image, $destination);
        }
        return $converters;
    }

    /**
     * @param OutputInterface $output
     * @param \Imagick        $image
     */
    protected function printImageInfo(OutputInterface $output, \Imagick $image)
    {
        $identify = $image->identifyimage(true);
        $colorSpace = $identify['colorSpace'];
        $iccProfile = preg_match('/icc:model: ([\S].*)/i', $identify['rawOutput'], $iccMatches);

        $output->writeln(sprintf('<fg=yellow>Image:</fg=yellow> %s', $identify['imageName']));
        $output->writeln(sprintf('<fg=yellow>Compression:</fg=yellow> %s', $identify['compression']));
        $output->writeln(sprintf('<fg=yellow>Mime Type:</fg=yellow> %s', $identify['mimetype']));
        $output->writeln(sprintf('<fg=yellow>Size:</fg=yellow> %s x %s pixels', $identify['geometry']['width'], $identify['geometry']['height']));
        $output->writeln(sprintf('<fg=yellow>Resolution:</fg=yellow> %s x %s DPI', $identify['resolution']['x'], $identify['resolution']['y']));
        $output->writeln(sprintf('<fg=yellow>ColorSpace:</fg=yellow> %s', $colorSpace));
        $output->writeln(sprintf('<fg=yellow>ICC Profile:</fg=yellow> %s', ($iccProfile ? $iccMatches[1] : 'no profile found')));
        $output->writeln('');
    }

} 