<?php

namespace ColorProfiles\Console\Command;

use ColorProfiles\ColorProfile;
use ColorProfiles\Container;
use ColorProfiles\Converter\ConverterInterface;
use Symfony\Component\Console\Command\Command as CliCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class Resize extends CliCommand
{

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this
            ->setName('resize')
            ->setDescription('Resize an image.')
            ->addArgument(
                'source',
                InputArgument::REQUIRED,
                'Source image'
            )
            ->addArgument(
                'destination',
                InputArgument::OPTIONAL,
                'Destination folder',
                sys_get_temp_dir()
            )
            ->addOption(
                'resolution',
                null,
                InputOption::VALUE_OPTIONAL,
                'Specify an output resolution',
                72
            )
            ->addOption(
                'width',
                null,
                InputOption::VALUE_OPTIONAL,
                'Specify an output width',
                800
            )
            ->addOption(
                'height',
                null,
                InputOption::VALUE_OPTIONAL,
                'Specify an output height',
                600
            )
            ->addOption(
                'format',
                null,
                InputOption::VALUE_OPTIONAL,
                'Specify an output format',
                'jpg'
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $image = $input->getArgument('source');
        $destination = $input->getArgument('destination');
        $resolution = $input->getOption('resolution');
        $width = $input->getOption('width');
        $height = $input->getOption('height');
        $format = $input->getOption('format');

        $destination = $this->createDestination($destination);
        $destinationFile = pathinfo($image, PATHINFO_FILENAME) . '.' . $format;
        $image = $this->loadImage($image);

        $output->writeln('<fg=green>INPUT FILE:</fg=green>');
        $this->printImageInfo($output, $image);

        // Handle transparent images:
        $image->setImageAlphaChannel(\Imagick::ALPHACHANNEL_OPAQUE  );
        $image->setImageBackgroundColor('white');
        $image->setImageMatteColor('white');
        $image->setImageMatte(true);

        $image->resizeimage($width, $height, \Imagick::FILTER_LANCZOS, 1);
        $image->setImageResolution($resolution, $resolution);
        $image->setImageFormat($format);

        $image->writeImage($destination . DIRECTORY_SEPARATOR . $destinationFile);

        // Print some extra file data:
        $output->writeln('<fg=green>OUTPUT FILE:</fg=green>');
        $this->printImageInfo($output, $image);
    }

    /**
     * @param $image
     *
     * @return \Imagick
     * @throws \RuntimeException
     */
    protected function loadImage($image)
    {
        $fileSystem = new Filesystem();
        if (!$fileSystem->exists($image)) {
            throw new \RuntimeException('Unable to locate image: ' . $image);
        }


        $imagick = new \Imagick($image);
        if ($imagick->getNumberImages() > 1) {
            $imagick->setIteratorIndex(0);
        }

        return $imagick;
    }

    /**
     * @param $destination
     *
     * @return string
     */
    protected function createDestination($destination)
    {
        $filesystem = new Filesystem();
        $filesystem->mkdir($destination);
        return realpath($destination);
    }

    /**
     * @param OutputInterface $output
     * @param \Imagick        $image
     */
    protected function printImageInfo(OutputInterface $output, \Imagick $image)
    {
        $identify = $image->identifyimage(true);
        $colorSpace = $identify['colorSpace'];
        $iccProfile = preg_match('/icc:model: ([\S].*)/i', $identify['rawOutput'], $iccMatches);

        $output->writeln(sprintf('<fg=yellow>Image:</fg=yellow> %s', $identify['imageName']));
        $output->writeln(sprintf('<fg=yellow>Compression:</fg=yellow> %s', $identify['compression']));
        $output->writeln(sprintf('<fg=yellow>Mime Type:</fg=yellow> %s', $identify['mimetype']));
        $output->writeln(sprintf('<fg=yellow>Size:</fg=yellow> %s x %s pixels', $identify['geometry']['width'], $identify['geometry']['height']));
        $output->writeln(sprintf('<fg=yellow>Resolution:</fg=yellow> %s x %s DPI', $identify['resolution']['x'], $identify['resolution']['y']));
        $output->writeln(sprintf('<fg=yellow>ColorSpace:</fg=yellow> %s', $colorSpace));
        $output->writeln(sprintf('<fg=yellow>Alpha chanel:</fg=yellow> %s', ($image->getImageAlphaChannel() ? 'yes' : 'no')));
        $output->writeln(sprintf('<fg=yellow>ICC Profile:</fg=yellow> %s', ($iccProfile ? $iccMatches[1] : 'no profile found')));
        $output->writeln('');
    }

} 