<?php

namespace ColorProfiles\Console;

use ColorProfiles\Console\Command;
use Symfony\Component\Console\Application as CliApplication;

/**
 * Class Application
 *
 * @package ColorProfiles\Console
 */
class Application extends CliApplication
{

    const APPLICATION_NAME = 'Color profile tester';
    const APPLICATION_VERSION = '0.1.0';

    /**
     * @param string $name
     * @param string $version
     */
    public function __construct($name = 'UNKNOWN', $version = 'UNKNOWN')
    {
        $name = self::APPLICATION_NAME;
        $version = self::APPLICATION_VERSION;

        parent::__construct($name, $version);
    }

    /**
     * @return \Symfony\Component\Console\Command\Command[]
     */
    protected function getDefaultCommands()
    {
        $commands = parent::getDefaultCommands();
        $commands[] = new Command\Convert();
        $commands[] = new Command\Resize();

        return $commands;
    }

}