<?php
/**
 * Phpro ZF2 Library
 *
 * @link      http://fisheye.phpro.be/git/Git-Vlir-Uos.git
 * @copyright Copyright (c) 2012 PHPro
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */
namespace ColorProfiles\Converter;
use ColorProfiles\Container;


/**
 * Class ImageConverter
 *
 * @package ColorProfiles\Converter
 */
interface ConverterInterface
{
    /**
     * Convert an asset with multiple conversion profiles
     *
     * @param        $profiles
     * @param string $outputType
     *
     * @return \Generator
     */
    public function convert(Container $profiles, $outputType = 'jpg');

    /**
     * Can the converter convert current image
     *
     * @return bool
     */
    public function canConvertImage();

}