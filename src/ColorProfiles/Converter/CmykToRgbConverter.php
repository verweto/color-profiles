<?php

namespace ColorProfiles\Converter;

use ColorProfiles\ColorProfile;
use ColorProfiles\Container;

/**
 * Class ImageConverter
 *
 * @package ColorProfiles\Converter
 */
final class CmykToRgbConverter
    implements ConverterInterface
{

    /**
     * @var \Imagick
     */
    protected $image;

    /**
     * @var string
     */
    protected $destination;

    /**
     * @param \Imagick $image
     * @param $destination
     * @throws \RuntimeException
     */
    public function __construct($image, $destination)
    {
        $this->image = $image;
        $this->destination = $destination;
    }

    /**
     * @return bool
     */
    public function canConvertImage()
    {
        return ($this->image->getimagecolorspace() == \Imagick::COLORSPACE_CMYK);
    }

    /**
     * @param Container $profiles
     * @param string    $outputType
     *
     * @return \Generator
     */
    public function convert(Container $profiles, $outputType = 'jpg')
    {
        $rgbProfiles = $profiles->findByColorSpace(ColorProfile::COLORSPACE_RGB);
        $cmykProfiles = $profiles->findByColorSpace(ColorProfile::COLORSPACE_CMYK);

        foreach ($cmykProfiles->getIterator() as $cmykProfile) {
            foreach ($rgbProfiles->getIterator() as $rgbProfile) {
                yield $this->convertSingle($cmykProfile, $rgbProfile, $outputType);
            }
        }
    }

    /**
     * @param \ColorProfiles\ColorProfile $cmykProfile
     * @param \ColorProfiles\ColorProfile $rgbProfile
     * @param string       $outputType
     *
     * @return string
     */
    protected function convertSingle(ColorProfile $cmykProfile, ColorProfile $rgbProfile, $outputType = 'jpg')
    {
        $image = clone $this->image;

        // Set current profiles:
        $image->profileImage('*', null);
        $image->profileImage($cmykProfile->getType(), $cmykProfile->getProfile());
        $image->profileImage($rgbProfile->getType(), $rgbProfile->getProfile());

        // Calculate destination
        $destination = sprintf('%s/%s-%s.%s', $this->destination, $cmykProfile, $rgbProfile, $outputType);

        // Convert
        $image->setColorspace(\Imagick::COLORSPACE_RGB);
        $image->stripImage();
        $image->writeImage($destination);
        $image->clear();

        return $destination;
    }

}
